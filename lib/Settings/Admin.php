<?php

namespace OCA\LauruxContact\Settings;

use \OCA\LauruxContact\Service\DatabaseService;

use OCP\Settings\ISettings;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\IDateTimeFormatter;
use OCP\IL10N;

class Admin implements ISettings
{
	protected $databaseService;
	private $l;

	public function __construct(DatabaseService $databaseService, IL10N $l10n) {
		$this->l = $l10n;
		$this->databaseService = $databaseService;
	}

    public function getForm() {
		$params = [
            "database" => $this->databaseService->getAppValue('database'),
            "ip" => $this->databaseService->getAppValue('ip'),
            "username" => $this->databaseService->getAppValue('username'),
            "password" => $this->databaseService->getAppValue('password'),
            "port" => $this->databaseService->getAppValue('port')
		];

		return new TemplateResponse("lauruxcontact", 'admin', $params);
	}


    public function getSection() {
		return "lauruxcontact";
    }
    
    public function getPriority() {
		return 25;
	}
}