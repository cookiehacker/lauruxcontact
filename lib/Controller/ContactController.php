<?php

/**
 * ===========================
 * 
 * Classe permettant la gestion de la partie contact de l'application
 * 
 * ===========================
 */

namespace OCA\LauruxContact\Controller;

use OCA\LauruxContact\Service\DatabaseService;
use OCA\LauruxContact\Constant\Contact;
use OCA\LauruxContact\Http\CSVDownloadResponse;

use OCP\IConfig;
use OCP\IRequest;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Http\DataResponse;
use OCP\AppFramework\Http\JSONResponse;
use OCP\AppFramework\Controller;
use OCP\AppFramework\Http\StrictContentSecurityPolicy;
use OCP\ILogger;
use OCP\Contacts\IManager;

use OC\DatabaseException;
use OC\DB\Connection;
use OC\DB\ConnectionFactory;

use Exception;

class ContactController extends Controller {
	private $userId;
	protected $databaseService;
	private $contactsManager;
	private $logger;
	private $config;
	protected $appName;

	public function __construct(
		string $AppName, 
		IRequest $request, 
		IManager $contactsManager, 
		ILogger $logger, 
		$UserId, 
		DatabaseService $databaseService,
		IConfig $config
	){
		parent::__construct($AppName, $request);
		$this->logger = $logger;
		$this->userId = $UserId;
		$this->databaseService = $databaseService;
		$this->contactsManager = $contactsManager;
		$this->config = $config;
		$this->appName = $AppName;
	}

	/**
	 * Fonction affichant la page d'accueil de l'application. Fonction permettant de se connecter à la base de donnée Laurux.
	 * 
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 */
	public function index() {
		$this->logger->info("Get data", array('DB' => 'My database'));

		$dbDriver = "mysql";
		$dbHostname = $this->databaseService->getAppValue('ip');
		$dbDatabase = $this->databaseService->getAppValue('database');
		$dbUsername = $this->databaseService->getAppValue('username');
		$dbPassword = $this->databaseService->getAppValue('password');
		$dbPort = $this->databaseService->getAppValue('port');
		$dbVerif = "None";
		$result;

		try{
			$connection = $this->getConnection($dbDriver, $dbHostname, $dbDatabase, $dbUsername, $dbPassword, $dbPort);
			
			$qdb = $connection->getQueryBuilder();

			//SELECT COUNT(cli_code) FROM Fiches_Cli
			$qdb->selectAlias(
					$qdb->createFunction('COUNT(cli_code)'), 'count'
				)
				->from('Fiches_Cli');
			$cursor = $qdb->execute();
			$row = $cursor->fetch();
			$cursor->closeCursor();

			$this->logger->info(array_values($row)[0], array('DB' => "My database"));
			$result = array_values($row)[0];
			
			$dbVerif = "OK";
		}
		catch (Exception $exception) {
			$dbVerif = "(user : ".$dbUsername.") ERROR : " . $exception->getMessage();
		}

		$a = $this->config->getAppValue($this->appName, "spectacle");
		$spe = array();
		if(isset($a))
		{
			$spe = explode(";", $a);
		}

		$contact = $this->contactsManager->search('', ['UID', 'EMAIL']);

		$params = [
			"driver" => $dbDriver,
            "database" => $dbDatabase,
            "ip" => $dbHostname,
            "username" => $dbUsername,
			"port" => $dbPort,
			"dbVerif" => $dbVerif,
			"result" => $result,
			"addressbook" => $this->contactsManager->getAddressBooks(),
			"contacts" => $contact,
			"spectacles" => $spe,
			"fcr" => Contact::CONTEXTESREPR
		];
		
		$response = new TemplateResponse('lauruxcontact', 'index', $params);

		$csp = new StrictContentSecurityPolicy();
		$csp->allowEvalScript();
		$csp->allowInlineStyle();

		$response->setContentSecurityPolicy($csp);

		return $response;
	}


	/**
	 * Récupération de la connexion vers une base de donnée
	 */
	private function getConnection($dbDriver, $dbHostname, $dbDatabase, $dbUsername, $dbPassword, $dbPort)
	{
		if (empty($dbDriver)) {
            throw new DatabaseException("No database driver specified.");
		}
		
		$connectionFactory = new ConnectionFactory(
            \OC::$server->getSystemConfig()
		);
		
		$parameters = [
            "host" => $dbHostname,
            "password" => $dbPassword,
            "user" => $dbUsername,
            "dbname" => $dbDatabase,
            "tablePrefix" => ""
		];
		
		$connection = $connectionFactory->getConnection($dbDriver, $parameters);
		$connection->executeQuery("SELECT 'Fiches_Cli'");
		
		return $connection;
	}

	/**
	 * Fonction permettant d'importer un fichier CSV
	 */
	public function importCSV($csv)
	{
		$this->logger->info(implode(" -- ",$csv), array('CSV' => "My CSV FILE"));
	}

	/**
	 * Fonction permettant de récupérer un contact de Laurux via un identifiant
	 */
	public function getOnlyContact($id)
	{
		$arr = array();
		$arr['status'] = 'ok';
		
		$connection = $this->getConnection(
			"mysql", 
			$this->databaseService->getAppValue('ip'), 
			$this->databaseService->getAppValue('database'), 
			$this->databaseService->getAppValue('username'), 
			$this->databaseService->getAppValue('password'), 
			$this->databaseService->getAppValue('port')
		);

		$contacts = $this->contactsManager->search($filter ?: '', Contact::SEARCHPROPERTIES);

		$contact;

		foreach($contacts as $c)
		{
			if(strcmp($c['UID'], $id) == 0)
			{
				$contact = $c;
				break;
			}
		}

		if(isset($contact))
		{
			$nom = explode(" ", $contact['FN'])[1];
			$prenom = explode(" ", $contact['FN'])[0];
			$mail = $contact['EMAIL'];
			$spe = $contact['SPE'];

			$arr['uid'] = $id;
			$arr['nom'] = $nom;
			$arr['prenom'] = $prenom;
			$arr['mail'] = $mail;
			$arr['spe'] = $spe;
		}
		else
		{
			$arr['status'] = 'err';
		}

		return json_encode($arr);
	}

	/**
	 * Fonction permettant de synchroniser un contact nextcloud vers Laurux via son identifiant
	 */
	public function syncOnlyContact($id)
	{
		$connection = $this->getConnection(
			"mysql", 
			$this->databaseService->getAppValue('ip'), 
			$this->databaseService->getAppValue('database'), 
			$this->databaseService->getAppValue('username'), 
			$this->databaseService->getAppValue('password'), 
			$this->databaseService->getAppValue('port')
		);

		$contact = $this->contactsManager->search($id, ['UID'], ['types' => true])[0];

		if(isset($contact))
		{
			$nom = explode(" ", $contact['FN'])[1];
			$prenom = explode(" ", $contact['FN'])[0];
			$mail = explode("|",implode("|",$contact['EMAIL'][0]))[1];

			if(isset($contact['ADR']))
			{
				$homeadr = array();
				$workadr = array();

				for($i = 0; $i < sizeof($contact['ADR']); $i++)
				{
					if(explode("|",implode("|",$contact['ADR'][$i]))[0] == "WORK")
					{
						array_push($workadr, explode("|",implode("|",$contact['ADR'][$i]))[1]);
					}
					elseif (explode("|",implode("|",$contact['ADR'][$i]))[0] == "HOME") {
						array_push($homeadr, explode("|",implode("|",$contact['ADR'][$i]))[1]);
					}
				}

				if(sizeof($homeadr) > 0) if(strcmp(explode(";",$homeadr[0])[6], '') !== 0) $pays = explode(";",$homeadr[0])[6];
				if(sizeof($workadr) > 0) if(strcmp(explode(";",$homeadr[0])[6], '') !== 0) $pays = explode(";",$homeadr[0])[6];

				$fhome = explode(";",$homeadr[0]);
				$fwork = explode(";",$workadr[0]);

			}

			if(isset($contact['TEL']))
			{
				$homephone = array();
				$workphone = array();
				
				for($i = 0; $i < sizeof($contact['TEL']); $i++)
				{
					if(explode(",",implode(",",$contact['TEL'][$i]))[0] == "WORK")
					{
						array_push($workphone, explode("|",implode("|",$contact['TEL'][$i]))[1]);
					}
					elseif (explode(",",implode(",",$contact['TEL'][$i]))[0] == "HOME") {
						array_push($homephone, explode("|",implode("|",$contact['TEL'][$i]))[1]);
					}
				}
			}

			$qdb = $connection->getQueryBuilder();

			if(isset($contact['IDL']))
			{
				$idl = $contact['IDL'];

				$qdb->select($qdb->createFunction('COUNT(`cli_code`)', 'count'))
				->from('Fiches_Cli', 'fiche')
				->where('
					fiche.cli_code = :cli_code 
				')
				->setParameters(array(
					':cli_code' => $idl,
				));

				$cursor = $qdb->execute();
				$row = $cursor->fetch();
				$cursor->closeCursor();
				$result = array_values($row)[0];

				if($result == 0)
				{
					$change = array();
					$change['URI'] = $contact['URI'];
					$change['UID'] = $contact['UID'];

					$this->logger->info("ADD IN LAURUX", array('DB' => "My DB"));
					$qdbLID = $connection->getQueryBuilder();

					$qdbLID->select('fiche.cli_code')
							->from('Fiches_Cli', 'fiche')
							->orderby('fiche.cli_code', 'DESC')
							->setMaxResults(1);
					
					$cursorLID = $qdbLID->execute();
					$rowLID = $cursorLID->fetch();
					$cursorLID->closeCursor();
					$lastID = array_values($rowLID)[0];

					$qdb->insert('Fiches_Cli')
						->values(
							[
								'cli_code' => ':cli_code',
								'cli_nom' => ':cli_nom', 
								'cli_pnm' => ':cli_pnm', 
								'cli_email' => ':cli_email',
								'cli_tel_bur' => ':cli_tel_bur',
								'cli_tel_dom' => ':cli_tel_dom',
								'cli_adr1' => ':cli_adr1',
								'cli_cd_ptl' => ':cli_cd_ptl',
								'cli_ville' => ':cli_ville',
								'cli_adr12' => ':cli_adr12',
								'cli_cd_ptl2' => ':cli_cd_ptl2',
								'cli_ville2' => ':cli_ville2',
								'cli_pays' => ':cli_pays',
								'cli_rs_soc' => ':cli_rs_soc'
							]
						)
						->setParameter(':cli_nom', isset($nom)?$nom:"NULL")
						->setParameter(':cli_pnm', isset($prenom)?$prenom:"NULL")

						->setParameter(':cli_email', isset($mail)?$mail:"NULL")

						->setParameter(':cli_tel_bur', isset($workphone)? sizeof($workphone) > 0 ? $workphone[0]:"NULL":"NULL")
						->setParameter(':cli_tel_dom', isset($homephone)? sizeof($homephone) > 0 ? $homephone[0]:"NULL":"NULL")

						->setParameter(':cli_pays', isset($pays) ? $pays : "NULL")

						->setParameter(':cli_adr1',  isset($fwork) ? strcmp('', $fwork[2]) ? str_replace("\\", "", $fwork[2]) : "NULL" : "NULL")
						->setParameter(':cli_cd_ptl',  isset($fwork) ? strcmp('', $fwork[5]) ? str_replace("\\", "", $fwork[5]) : "NULL" : "NULL")
						->setParameter(':cli_ville', isset($fwork) ? strcmp('', $fwork[3]) ? str_replace("\\", "", $fwork[3]) : "NULL" : "NULL" )

						->setParameter(':cli_adr12', isset($fhome) ? strcmp('', $fhome[2]) ? str_replace("\\", "", $fwork[2]) : "NULL" : "NULL" )
						->setParameter(':cli_cd_ptl2', isset($fhome) ? strcmp('', $fhome[5]) ? str_replace("\\", "", $fwork[5]) : "NULL" : "NULL" )
						->setParameter(':cli_ville2', isset($fhome) ? strcmp('', $fhome[3]) ? str_replace("\\", "", $fwork[3]) : "NULL" : "NULL" )

						->setParameter(':cli_rs_soc', isset($contact['ORG']) ? $contact['ORG'] : "NULL")

						->setParameter(':cli_code', isset($lastID)?$lastID+1:"NULL");
						
					$qdb->execute();

					$change['IDL'] = $lastID+1;

					$this->contactsManager->createOrUpdate($change, $contact['addressbook-key']);
				}
				else
				{
					$this->logger->info("UPDATE IN LAURUX", array('DB' => "My DB"));
					$qdb->update('Fiches_Cli')
						->set('cli_nom',$qdb->expr()->literal(isset($nom)?$nom:"NULL"))
						->set('cli_pnm',$qdb->expr()->literal(isset($prenom)?$prenom:"NULL"))
						->set('cli_email',$qdb->expr()->literal(isset($mail)?$mail:"NULL"))

						->set('cli_tel_bur',$qdb->expr()->literal(isset($workphone)? sizeof($workphone) > 0 ? $workphone[0]:"NULL":"NULL"))
						->set('cli_tel_dom',$qdb->expr()->literal(isset($homephone)? sizeof($homephone) > 0 ? $homephone[0]:"NULL":"NULL"))

						->set('cli_adr1',$qdb->expr()->literal(isset($fwork) ? strcmp('', $fwork[2]) ? str_replace("\\", "", $fwork[2]) : "NULL" : "NULL"))
						->set('cli_cd_ptl',$qdb->expr()->literal(isset($fwork) ? strcmp('', $fwork[5]) ? str_replace("\\", "", $fwork[5]) : "NULL" : "NULL"))
						->set('cli_ville',$qdb->expr()->literal(isset($fwork) ? strcmp('', $fwork[3]) ? str_replace("\\", "", $fwork[3]) : "NULL" : "NULL"))

						->set('cli_adr12',$qdb->expr()->literal(isset($fhome) ? strcmp('', $fhome[2]) ? str_replace("\\", "", $fwork[2]) : "NULL" : "NULL"))
						->set('cli_cd_ptl2',$qdb->expr()->literal(isset($fhome) ? strcmp('', $fhome[5]) ? str_replace("\\", "", $fwork[5]) : "NULL" : "NULL"))
						->set('cli_ville2',$qdb->expr()->literal(isset($fhome) ? strcmp('', $fhome[3]) ? str_replace("\\", "", $fwork[3]) : "NULL" : "NULL"))

						->set('cli_rs_soc',$qdb->expr()->literal(isset($contact['ORG']) ? $contact['ORG'] : "NULL"))

						->set('cli_pays',$qdb->expr()->literal(isset($pays)?$pays:"NULL"))
						->where('cli_code = :cli_code')
						->setParameter(':cli_code', $idl);

					$qdb->execute();
				}
			}
			else
			{
				$change = array();
				$change['URI'] = $contact['URI'];
				$change['UID'] = $contact['UID'];

				$this->logger->info("ADD IN LAURUX", array('DB' => "My DB"));
				$qdbLID = $connection->getQueryBuilder();

				$qdbLID->select('fiche.cli_code')
						->from('Fiches_Cli', 'fiche')
						->orderby('fiche.cli_code', 'DESC')
						->setMaxResults(1);
						
				$cursorLID = $qdbLID->execute();
				$rowLID = $cursorLID->fetch();
				$cursorLID->closeCursor();
				$lastID = array_values($rowLID)[0];

				$qdb->insert('Fiches_Cli')
					->values(
						[
							'cli_code' => ':cli_code',
							'cli_nom' => ':cli_nom', 
							'cli_pnm' => ':cli_pnm', 
							'cli_email' => ':cli_email',
							'cli_tel_bur' => ':cli_tel_bur',
							'cli_tel_dom' => ':cli_tel_dom',
							'cli_adr1' => ':cli_adr1',
							'cli_cd_ptl' => ':cli_cd_ptl',
							'cli_ville' => ':cli_ville',
							'cli_adr12' => ':cli_adr12',
							'cli_cd_ptl2' => ':cli_cd_ptl2',
							'cli_ville2' => ':cli_ville2',
							'cli_pays' => ':cli_pays',
							'cli_rs_soc' => ':cli_rs_soc'
						]
					)
					->setParameter(':cli_nom', isset($nom)?$nom:"NULL")
					->setParameter(':cli_pnm', isset($prenom)?$prenom:"NULL")
					->setParameter(':cli_email', isset($mail)?$mail:"NULL")

					->setParameter(':cli_tel_bur', isset($workphone)? sizeof($workphone) > 0 ? $workphone[0]:"NULL":"NULL")
					->setParameter(':cli_tel_dom', isset($homephone)? sizeof($homephone) > 0 ? $homephone[0]:"NULL":"NULL")
					
					->setParameter(':cli_pays', isset($pays) ? $pays : "NULL")

					->setParameter(':cli_adr1',  isset($fwork) ? strcmp('', $fwork[2]) ? str_replace("\\", "", $fwork[2]) : "NULL" : "NULL")
					->setParameter(':cli_cd_ptl',  isset($fwork) ? strcmp('', $fwork[5]) ? str_replace("\\", "", $fwork[5]) : "NULL" : "NULL")
					->setParameter(':cli_ville', isset($fwork) ? strcmp('', $fwork[3]) ? str_replace("\\", "", $fwork[3]) : "NULL" : "NULL" )

					->setParameter(':cli_adr12', isset($fhome) ? strcmp('', $fhome[2]) ? str_replace("\\", "", $fwork[2]) : "NULL" : "NULL" )
					->setParameter(':cli_cd_ptl2', isset($fhome) ? strcmp('', $fhome[5]) ? str_replace("\\", "", $fwork[5]) : "NULL" : "NULL" )
					->setParameter(':cli_ville2', isset($fhome) ? strcmp('', $fhome[3]) ? str_replace("\\", "", $fwork[3]) : "NULL" : "NULL" )

					->setParameter(':cli_rs_soc', isset($contact['ORG']) ? $contact['ORG'] : "NULL")

					->setParameter(':cli_code', isset($lastID)?$lastID+1:"NULL");
							
				$qdb->execute();

				$change['IDL'] = $lastID+1;

				$this->contactsManager->createOrUpdate($change, $contact['addressbook-key']);
			}
		}
	}	

	/**
	 * Fonction permettant d'ajouter un spectacle dans Nextcloud
	 */
	public function ajoutSpectacle($spectacle)
	{
		$data['status'] = "ok";
		$data['contact'] = array();

		$contacts = $this->contactsManager->search($filter ?: '', ['UID'], ['types' => true]);

		if(isset($contacts))
		{
			foreach($contacts as $c)
			{
				$result = array();

				array_push($result, $c['FN'], $c['UID'], $c['EMAIL'], $c['SPE']);
				array_push($data['contact'], $result);
			}
		}
		else
		{
			$data['status'] = 'err';
		}

		$a = $this->config->getAppValue($this->appName, "spectacle");

		if(isset($a) && strcmp($a, '') != 0)
		{
			if(strpos($a,$spectacle) == false && strpos($spectacle,";") == false) $a = $a.";".$spectacle;
			else $data['status'] = 'err';
		}
		else
		{
			if(strpos($spectacle,";") == false) $a = $spectacle;
			else $data['status'] = 'err';
		}
		
		$data['spectacle'] = $a;
		
		$this->config->setAppValue($this->appName, "spectacle", $a);

		return json_encode($data);
	}

	/**
	 * Fonction permettant de filtrer les contacts via un nom de spectacle
	 */
	public function filtrerContactSpe($spectacle)
	{
		$data = array();
		$data['status'] = 'ok';
		$data['spectacle'] = $spectacle;
		$data['cat'] = Contact::SPECTACLE_CAT;

		foreach(Contact::SPECTACLE_CAT as $specat) $data['contact'][$specat[0]] = array();

		$contacts = $this->contactsManager->search('', ['UID'], ['types' => true]);

		if(isset($contacts))
		{
			foreach($contacts as $c)
			{
				foreach(Contact::SPECTACLE_CAT as $specat)
				{
					if(isset($c[$specat[0]])) $data = $this->filtreParSpe($spectacle, $specat[0], $data, 'contact', $c);
				}
			}
		}
		else
		{
			$data['status'] = 'err';
		}


		$this->logger->info(json_encode($data), array('Spectacle' => 'Mon spectacle'));

		return json_encode($data);
	}

	/**
	 * Fonction permettant de filtrer par spectacle
	 */
	private function filtreParSpe($spectacle, $clesSpe, $data, $clesData, $contact)
	{
		$spe = explode(";", $contact[$clesSpe]);
		if(in_array($spectacle, $spe))
		{
			$result = array();

			array_push($result, $contact['FN'], $contact['UID'], $contact['EMAIL'], $contact[$clesSpe]);
			array_push($data[$clesData][$clesSpe], $result);
		}
		return $data;
	}

	/**
	 * Fonction permettant de filtrer les contacts via un spectacle sans type (DEPRECATED)
	 */
	public function filtrerContactSP()
	{
		$data = array();
		$data['status'] = 'ok';
		$data['contact'] = array();

		$contacts = $this->contactsManager->search('', ['UID'], ['types' => true]);

		if(isset($contacts))
		{
			foreach($contacts as $c)
			{
				if(isset($c['ETSP']))
				{
					if(intval($c['ETSP'] == 1))
					{
						$result = array();

						array_push($result, $c['FN'], $c['UID'], $c['EMAIL'], $c['SPE']);
						array_push($data['contact'], $result);
					}
				}
			}
		}
		else
		{
			$data['status'] = 'err';
		}

		return json_encode($data);
	}

	/**
	 * Fonction permettant de filtrer un contact via un mot
	 */
	public function filtrerContact($filtre)
	{
		$data = array();
		$data['status'] = 'ok';
		$data['filtre'] = $filtre;
		$data['contact'] = array();

		$contacts = $this->contactsManager->search($filtre, Contact::SEARCHPROPERTIES, ['types' => true]);

		if(isset($contacts))
		{
			foreach($contacts as $c)
			{
				$result = array();

				array_push($result, $c['FN'], $c['UID'], $c['EMAIL'], $c['SPE']);
				array_push($data['contact'], $result);
			}
		}
		else
		{
			$data['status'] = 'err';
		}

		$this->logger->info(json_encode($data), array('Spectacle' => 'Mon spectacle'));

		return json_encode($data);
	}

	/**
	 * Fonction permettant de filtrer les contacts via le contexte de representation
	 */
	public function filtrerContactFCR($rep)
	{
		$data = array();
		$data['status'] = 'ok';
		$data['rep'] = $rep;
		$data['contact'] = array();

		$contacts = $this->contactsManager->search('', ['UID'], ['types' => true]);

		if(isset($contacts))
		{
			foreach($contacts as $c)
			{
				$arrayrep = explode(";", $c['CR']);
				if(in_array($rep, $arrayrep))
				{
					$result = array();

					array_push($result, $c['FN'], $c['UID'], $c['EMAIL']);
					array_push($data['contact'], $result);
				}
			}
		}
		else
		{
			$data['status'] = 'err';
		}

		return json_encode($data);
	}

	/**
	 * Fonction permettant d'annuler le filtrage des contacts
	 */
	public function annulerFiltre()
	{
		$data['status'] = "ok";
		$data['contact'] = array();

		$contacts = $this->contactsManager->search($filter ?: '', ['UID'], ['types' => true]);

		if(isset($contacts))
		{
			foreach($contacts as $c)
			{
				$result = array();

				array_push($result, $c['FN'], $c['UID'], $c['EMAIL'], $c['SPE']);
				array_push($data['contact'], $result);
			}
		}
		else
		{
			$data['status'] = 'err';
		}

		$a = $this->config->getAppValue($this->appName, "spectacle");

		$data['spectacle'] = $a;

		return json_encode($data);
	}

	/**
	 * Fonction permettant de supprimer un spectacle
	 */
	public function suppSpectacle($spectacle)
	{
		$data['status'] = "ok";
		$data['spectacle'] = '';
		$data['contact'] = array();

		$contacts = $this->contactsManager->search('', ['UID'], ['types' => true]);

		if(isset($contacts))
		{
			foreach($contacts as $c)
			{	
				$modif = false;

				$change = array();
				$change['URI'] = $c['URI'];
				$change['UID'] = $c['UID'];

				foreach(Contact::SPECTACLE_CAT as $specat)
				{
					if(isset($c[$specat[0]]))
					{
						$spearr = explode(";",$c[$specat[0]]);
						if(sizeof($spearr) > 0)
						{
							$speindex = array_search($spectacle, $spearr);
							if(isset($speindex))
							{
								$modif = true;
								unset($spearr[$speindex]);
								$speresult = implode(";", $spearr);
								$change[$specat[0]] = $speresult;
							}
						}
					}
				}

				if($modif) $this->contactsManager->createOrUpdate($change, $c['addressbook-key']);

				$result = array();

				array_push($result, $c['FN'], $c['UID'], $c['EMAIL'], $c['SPE']);
				array_push($data['contact'], $result);
				
			}
		}
		else
		{
			$data['status'] = 'err';
		}

		$a = $this->config->getAppValue($this->appName, "spectacle");

		if(isset($a))
		{
			$arr = explode(";", $a);
			if(sizeof($arr) > 0)
			{
				$index = array_search($spectacle, $arr);
				if(isset($index))
				{
					unset($arr[$index]);
					$result = implode(";", $arr);
					$data['spectacle'] = $result;
					$this->config->setAppValue($this->appName, "spectacle", $result);
				}
				else
				{
					$data['status'] = "err";
				}
			}
			else
			{
				if(strcmp($a, $spectacle) != 0) $this->config->setAppValue($this->appName, "spectacle", '');
				else $data['status'] = "err";
			}
		}
		else $data['status'] = "err";

		return json_encode($data);
	}

	/**
	 * Fonction permettant la suppression d'un contact
	 * 
	 * @NoAdminRequired
	 */
	public function suppContact($utilisateur)
	{		
		$dbDriver = "mysql";
		$dbHostname = $this->config->getSystemValue('dbhost');
		$dbDatabase = $this->config->getSystemValue('dbname');
		$dbUsername = $this->config->getSystemValue('dbuser');
		$dbPassword = $this->config->getSystemValue('dbpassword');
		$dbPort = $this->config->getSystemValue('3306');

		$connection = $this->getConnection($dbDriver, $dbHostname, $dbDatabase, $dbUsername, $dbPassword, $dbPort);

		$qdb = $connection->getQueryBuilder();

		$qdb->delete('oc_cards')
			->where('
				uid = :uid 
			')
			->setParameters(array(
				':uid' => $utilisateur,
			));

		$qdb->execute();

		$data['status'] = "ok";
		$data['contact'] = array();

		$contacts = $this->contactsManager->search('', ['UID'], ['types' => true]);

		if(isset($contacts))
		{
			foreach($contacts as $c)
			{
				$result = array();

				array_push($result, $c['FN'], $c['UID'], $c['EMAIL'], $c['SPE']);
				array_push($data['contact'], $result);
			}
		}
		else
		{
			$data['status'] = 'err';
		}

		return json_encode($data);
	}

	/**
	 * Fonction permettant de sauvegarder les contacts en format CSV
	 * 
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 * @TrapError
	 */
	public function saveInCSV()
	{
		$a = $this->config->getAppValue($this->appName, "spectacle");
		$spe = array();
		if(isset($a))
		{
			$spe = explode(";", $a);
		}

		$header  = array(
			"prenom",
			"nom",
			"societe",
			"titre",
			"Telephone bureau",
			"Telephone portable",
			"Adresse",
			"Code postal",
			"Ville",
			"Département",
			"Pays",
			"Contexte de représentation",
			"Contact prévu",
			"Note",
			"Suivi de prestations",
			"Historique"
		);
		if(isset($spe)) foreach($spe as $s) array_push($header, $s);

		$csv = array(
			$header
		);

		$filename = "all_contacts_export_" . date("Y-m-d") . ".csv";

		$contacts = $this->contactsManager->search('', ['UID'], ['types' => true]);

		if(isset($contacts))
		{
			foreach($contacts as $contact)
			{
				$homephone = array();
				$workphone = array();
		
				for($i = 0; $i < sizeof($contact['TEL']); $i++)
				{
					if(explode(",",implode(",",$contact['TEL'][$i]))[0] == "WORK")
					{
						array_push($workphone, explode("|",implode("|",$contact['TEL'][$i]))[1]);
					}
					elseif (explode(",",implode(",",$contact['TEL'][$i]))[0] == "HOME") {
						array_push($homephone, explode("|",implode("|",$contact['TEL'][$i]))[1]);
					}
				}
		
				$workadr = array();
		
				for($i = 0; $i < sizeof($contact['ADR']); $i++)
				{
					if(explode("|",implode("|",$contact['ADR'][$i]))[0] == "WORK")
					{
						array_push($workadr, explode("|",implode("|",$contact['ADR'][$i]))[1]);
					}
				}
		
				$arrWA = array();
				empty($workadr[0]) ?: $arrWA = explode(";", $workadr[0]); 
		
				
		
				$value = array(
					empty($contact['FN']) ? "" : explode(" ", $contact['FN'])[0],
					empty($contact['FN']) ? "" : explode(" ", $contact['FN'])[1],
					empty($contact['ORG']) ? "" : $contact['ORG'],
					empty($contact['TITLE']) ? "" :$contact['TITLE'],
					empty($workphone[0]) ? "" : $workphone[0],
					empty($homephone[0]) ? "" : $homephone[0],
					empty($arrWA[2]) ? "" : str_replace("\\", "", $arrWA[2]),
					empty($arrWA[5]) ? "" : str_replace("\\", "", $arrWA[5]),
					empty($arrWA[3]) ? "" : str_replace("\\", "", $arrWA[3]),
					empty($arrWA[4]) ? "" : str_replace("\\", "", $arrWA[4]),
					empty($arrWA[6]) ? "" : str_replace("\\", "", $arrWA[6]),
					empty($contact['CR']) ? "" : implode(" / ",explode(";", $contact['CR'])),
					empty($contact["CP"]) ? "" : date("d/m/Y", strtotime($contact["CP"])),
					empty($contact['NOTE']) ? "" : $contact['NOTE'],
					empty($contact['SP']) ? "" : $contact['SP'],
					empty($contact['HIST']) ? "" : $contact['HIST']
				);
		
				if(isset($spe)) foreach($spe as $s) array_push($value, $this->getEtatSpectacle($contact, $s));

				array_push($csv, $value);
			}
		}

		$res = "";
        
        foreach($csv as $row)
        {
			$res = $res . implode(",", $row) . "\n";
		}
		

		return new CSVDownloadResponse($res, $filename, 'application/octet-stream');
	}

	/**
	 * Fonction permettant de récupérer la catégorie d'un spectacle dans un contact
	 */
	private function getEtatSpectacle($contact, $spectacle) : string
	{
		foreach(CONTACT::SPECTACLE_CAT as $specat)
		{
			if(in_array($spectacle, explode(";",  $contact[$specat[0]]))) return $specat[1];
		}
		return "pas d'avis";
	}

}
