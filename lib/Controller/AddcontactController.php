<?php

/**
 * ===========================
 * 
 * Classe permettant d'ajouter un contact dans la base de données Nextcloud
 * 
 * ===========================
 */

namespace OCA\LauruxContact\Controller;

use OCA\LauruxContact\Constant\Contact;

use OCP\IConfig;
use OCP\IRequest;

use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Http\DataResponse;
use OCP\AppFramework\Http\JSONResponse;
use OCP\AppFramework\Controller;
use OCP\AppFramework\Http\StrictContentSecurityPolicy;

use OCP\ILogger;
use OCP\Contacts\IManager;

class AddcontactController extends Controller {

    private $userId;
	private $contactsManager;
	private $logger;
	private $config;
	protected $appName;

	public function __construct(
		string $AppName, 
		IRequest $request, 
		IManager $contactsManager, 
		ILogger $logger, 
		$UserId, 
		IConfig $config
	){
		parent::__construct($AppName, $request);
		$this->logger = $logger;
		$this->userId = $UserId;
		$this->contactsManager = $contactsManager;
		$this->config = $config;
		$this->appName = $AppName;
    }

	/**
	 * Affichage de la page d'ajout d'un contact dans Nextcloud
	 * 
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 */
    public function index()
    {
		$params = [
			"contexte" => Contact::CONTEXTESREPR,
		];

        $response = new TemplateResponse('lauruxcontact', 'ajouterContact', $params);

		$csp = new StrictContentSecurityPolicy();
		$csp->allowEvalScript();
		$csp->allowInlineStyle();

		$response->setContentSecurityPolicy($csp);

		return $response;
	}
	
	/**
	 * Fonction permettant d'ajouter un contact
	 */
	public function ajouterContact($contact)
	{
		$arr = array();

		$newContact = $this->contactsManager->createOrUpdate($this->createNewContact(json_decode($contact)), 1);

		$arr['contactID'] = $newContact['UID'];

		return json_encode($arr);
	}

	/**
	 * Fonction permettant de verifier les valeurs rentrée et remplacer les vides par 'undefined'
	 */
	private function createNewContact($newContact)
	{
		$result = array();
		foreach($newContact as $key => $value)
		{
			if(strcmp($value, "") !== 0 && strcmp($value, "undefined") !== 0)
			{
				$result[$key] = $value;
			}
		}
		return $result;
	}
}