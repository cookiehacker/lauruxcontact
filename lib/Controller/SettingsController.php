<?php

/**
 * ===========================
 * 
 * Classe permettant la configuration de l'application
 * 
 * ===========================
 */

namespace OCA\LauruxContact\Controller;

use \OCA\LauruxContact\Service\DatabaseService;

use \OCP\IRequest;
use \OCP\AppFramework\Http\TemplateResponse;
use \OCP\AppFramework\Controller;
use \OCP\AppFramework\Http\JSONResponse;
use \OCP\AppFramework\Http;


class SettingsController extends Controller {

	private $userId;
      protected $databaseService;

	public function __construct ($appName, IRequest $request, $userId, DatabaseService $databaseService) {
		parent::__construct($appName, $request);
		$this->userId = $userId;
		$this->databaseService = $databaseService;
    }

    /**
     * Fonction permettant d'appliquer la nouvelle configuration de la base de données Laurux dans l'application
     */
	public function configurationSet ($database, $ip, $username, $password, $port) {
            $this->databaseService->setAppValue('database', $database);
            $this->databaseService->setAppValue('ip', $ip);
            $this->databaseService->setAppValue('username', $username);
            $this->databaseService->setAppValue('password', $password);
            $this->databaseService->setAppValue('port', $port);
            return new JSONResponse(array(
                  "database" => $this->databaseService->getAppValue('database'),
                  "ip" => $this->databaseService->getAppValue('ip'),
                  "username" => $this->databaseService->getAppValue('username'),
                  "password" => $this->databaseService->getAppValue('password'),
                  "port" => $this->databaseService->getAppValue('port')
                  ));
      }
};
?>