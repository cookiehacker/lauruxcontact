<?php

/**
 * ===========================
 * 
 * Classe permettant de modifier / afficher un contact dans la base de données Nextcloud
 * 
 * ===========================
 */

namespace OCA\LauruxContact\Controller;

use OCA\LauruxContact\Constant\Contact;
use OCA\LauruxContact\Http\CSVDownloadResponse;

use OCP\IConfig;
use OCP\IRequest;
use OCP\AppFramework\Http\TemplateResponse;
use OCP\AppFramework\Http\DataResponse;
use OCP\AppFramework\Http\JSONResponse;
use OCP\AppFramework\Controller;
use OCP\AppFramework\Http\StrictContentSecurityPolicy;
use OCP\ILogger;
use OCP\Contacts\IManager;

class EditcontactController extends Controller {
	private $userId;
	private $contactsManager;
	private $logger;
	private $config;
	protected $appName;

	public function __construct(
		string $AppName, 
		IRequest $request, 
		IManager $contactsManager, 
		ILogger $logger, 
		$UserId, 
		IConfig $config
	){
		parent::__construct($AppName, $request);
		$this->logger = $logger;
		$this->userId = $UserId;
		$this->contactsManager = $contactsManager;
		$this->config = $config;
		$this->appName = $AppName;
    }

    /**
	 * Affichage de la page de modification / d'affichage d'un contact dans Nextcloud
	 * 
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 */
    public function index($id)
    {
		$spectacleArray = array();
		$paramSpe = array();

		$contact = $this->contactsManager->search($id, ['UID'], ['types' => true])[0];

		$a = $this->config->getAppValue($this->appName, "spectacle");
		$spe = array();
		if(isset($a))
		{
			$spe = explode(";", $a);
		}

		foreach(CONTACT::SPECTACLE_CAT as $specat)
		{
			$paramSpe[$specat[0]] = $contact[$specat[0]];
		}

		$date = isset($contact["CP"]) ? $contact["CP"] : "";

		$homephone = array();
		$workphone = array();

		for($i = 0; $i < sizeof($contact['TEL']); $i++)
		{
			if(explode(",",implode(",",$contact['TEL'][$i]))[0] == "WORK")
			{
				array_push($workphone, explode("|",implode("|",$contact['TEL'][$i]))[1]);
			}
			elseif (explode(",",implode(",",$contact['TEL'][$i]))[0] == "HOME") {
				array_push($homephone, explode("|",implode("|",$contact['TEL'][$i]))[1]);
			}
		}

		$workadr = array();

		for($i = 0; $i < sizeof($contact['ADR']); $i++)
		{
			if(explode("|",implode("|",$contact['ADR'][$i]))[0] == "WORK")
			{
				array_push($workadr, explode("|",implode("|",$contact['ADR'][$i]))[1]);
			}
		}

		$homeemail = array();
		$workemail = array();

		for($i = 0; $i < sizeof($contact['EMAIL']); $i++)
		{
			if(explode(",",implode(",",$contact['EMAIL'][$i]))[0] == "WORK")
			{
				array_push($workemail, explode("|",implode("|",$contact['EMAIL'][$i]))[1]);
			}
			elseif (explode(",",implode(",",$contact['EMAIL'][$i]))[0] == "HOME") {
				array_push($homeemail, explode("|",implode("|",$contact['EMAIL'][$i]))[1]);
			}
		}

    	$params = [
			"contacts" => $contact,

			"spectacles" => $spe,

			"contexte" => Contact::CONTEXTESREPR,

			"date" => $date,

			"workphone" => $workphone[0],
			"homephone" => $homephone[0],

			"workadr" => $workadr[0],

			"workemail" => $workemail[0],
			"homeemail" => $homeemail[0],

			"cat" => CONTACT::SPECTACLE_CAT,

			"spe" => $paramSpe
		];


        $response = new TemplateResponse('lauruxcontact', 'editcontact', $params);

		$csp = new StrictContentSecurityPolicy();
		$csp->allowEvalScript();
		$csp->allowInlineStyle();

		$response->setContentSecurityPolicy($csp);

		return $response;
	}

	/**
	 * Fonction permettant de modifier un contact
	 */
	public function modifier($contactedited, $utilisateur)
	{
		$data = array();
		$data['status'] = 'ok';
		$data['id'] = $utilisateur;

		$contact = $this->contactsManager->search($utilisateur, ['UID'], ['types' => true])[0];

		if(isset($contact) != 0)
		{
			$ce = $this->editContact(json_decode($contactedited));

			$change['URI'] = $contact['URI'];

			$change['ADR'] = array();
			$change['EMAIL'] = array();
			$change['TEL'] = array();

			$change['ADR;TYPE=WORK'] = array();
			$change['EMAIL;TYPE=HOME'] = array();
			$change['EMAIL;TYPE=WORK'] = array();
			$change['TEL;TYPE="WORK,VOICE"'] = array();
			$change['TEL;TYPE="HOME,VOICE"'] = array();

			array_merge($change, $ce);

			$change['ADR;TYPE=WORK'] = $ce['ADR;TYPE=WORK'];
			$change['EMAIL;TYPE=HOME'] = $ce['EMAIL;TYPE=HOME'];
			$change['EMAIL;TYPE=WORK'] = $ce['EMAIL;TYPE=WORK'];
			$change['TEL;TYPE="WORK,VOICE"'] = $ce['TEL;TYPE="WORK,VOICE"'];
			$change['TEL;TYPE="HOME,VOICE"'] = $ce['TEL;TYPE="HOME,VOICE"'];

			$change["ETSP"] = $ce["ETSP"];

			$this->contactsManager->createOrUpdate($change, $contact['addressbook-key']);
		}
		else
		{
			$data['status'] = 'err';
		}
		
		return json_encode($data);
	}

	/**
	 * Fonction permettant de verifier les valeurs rentrée et remplacer les vides par 'undefined'
	 */
	private function editContact($contact)
	{
		$result = array();
		foreach($contact as $key => $value)
		{
			if(strcmp($value, "") !== 0 && strcmp($value, "undefined") !== 0)
			{
				$result[$key] = $value;
				$this->logger->info($key . " --> " . $value, array('CONTACT' => 'My CONTACT'));
			}
		}
		return $result;
	}
	
	/**
	 * Fonction permettant d'ajouter le contact dans un spectacle
	 */
	public function ajoutContactDansSpe($utilisateur, $spectacle, $cat)
	{
		$data = array();
		$data['status'] = 'ok';
		$data['id'] = $utilisateur;
		$data['cat'] = CONTACT::SPECTACLE_CAT;

		$contact = $this->contactsManager->search($utilisateur, ['UID'], ['types' => true])[0];

		if(isset($contact) != 0)
		{
			$change = array();
			$change['URI'] = $contact['URI'];
			$change['UID'] = $contact['UID'];

			$change = $this->addSpeInList($spectacle, $cat, $change, $contact);
			foreach(CONTACT::SPECTACLE_CAT as $specat)
			{
				if(strcmp($cat, $specat[0] !== 0)) $change = $this->checkOccSpectacle($spectacle, $specat[0], $change, $contact);
			}

			$this->contactsManager->createOrUpdate($change, $contact['addressbook-key']);

			$contact = $this->contactsManager->search($utilisateur, ['UID'], ['types' => true])[0];
			
			foreach(CONTACT::SPECTACLE_CAT as $specat)
			{
				if(isset($contact[$specat[0]])) $data[$specat[0]] = $contact[$specat[0]];
			}
		}
		else
		{
			$data['status'] = 'err';
		}

		return json_encode($data);
	}

	/**
	 * Fonction permettant de supprimer le contact du spectacle
	 */
	public function suppSpeDansContact($utilisateur, $spectacle)
	{
		$data = array();
		$data['status'] = 'ok';
		$data['id'] = $utilisateur;
		$data['cat'] = CONTACT::SPECTACLE_CAT;

		$contact = $this->contactsManager->search($utilisateur, ['UID'], ['types' => true])[0];

		if(isset($contact) != 0)
		{
			$change = array();
			$change['URI'] = $contact['URI'];
			$change['UID'] = $contact['UID'];

			$cles = $this->findSpectacle($contact, $spectacle);

			if(isset($cles))
			{
				if(isset($contact[$cles]) != 0)
				{
					$arr = explode(";", $contact[$cles]);
					$index = array_search($spectacle, $arr);
					if(isset($index))
					{
						unset($arr[$index]);
						$change[$cles] =  implode(";", $arr);
					}
					$data[strtolower($cles)] = $change[$cles];
					$this->contactsManager->createOrUpdate($change, $contact['addressbook-key']);

					$contact = $this->contactsManager->search($utilisateur, ['UID'], ['types' => true])[0];
					
					foreach(Contact::SPECTACLE_CAT as $specat)
					{
						if(isset($contact[$specat[0]])) $data[$specat[0]] = $contact[$specat[0]];
					}
				}
				else
				{
					$data['status'] = 'err';
				}
			}
			else
			{
				$data['status'] = 'err';
			}
		}
		else
		{
			$data['status'] = 'err';
		}

		return json_encode($data);
	}

	/**
	 * Fonction permettant de verifier une eventuel occurence de spectacle dans le contact
	 */
	private function checkOccSpectacle($spectacle, $cles, $change, $contact)
	{
		if(in_array($spectacle, explode(";",$contact[$cles])))
		{
			$arr = explode(";", $contact[$cles]);
			$index = array_search($spectacle, $arr);
			if(isset($index))
			{
				unset($arr[$index]);
				$change[$cles] =  implode(";", $arr);
			}
		}

		return $change;
	}

	/**
	 * Fonction permettant d'ajout le spectacle dans la liste des spectacles dans le contact
	 */
	private function addSpeInList($spectacle, $cles, $change, $contact)
	{
		if(isset($contact[$cles]) != 0)
		{
			if(!in_array($spectacle, explode(";",$contact[$cles]))) $change[$cles] = $contact[$cles].$spectacle.";";
			else $change[$cles] = $contact[$cles];
		}
		else
		{
			$change[$cles] = $spectacle.";";
		}
		return $change;
	}

	/**
	 * Fonction permettant de trouver les spectacles dans le contact
	 */
	private function findSpectacle($contact, $spectacle) : string
	{
		foreach(Contact::SPECTACLE_CAT as $specat)
		{
			if(in_array($spectacle, explode(";",  $contact[$specat[0]]))) return $specat[0];
		}
		return null;
	}

	/**
	 * Fonction permettant de sauvegarder le contact dans un fichier CSV
	 * 
	 * @NoAdminRequired
	 * @NoCSRFRequired
	 * @TrapError
	 */
	public function saveInCSV($id)
	{
		$contact = $this->contactsManager->search($id, ['UID'], ['types' => true])[0];

		$homephone = array();
		$workphone = array();

		for($i = 0; $i < sizeof($contact['TEL']); $i++)
		{
			if(explode(",",implode(",",$contact['TEL'][$i]))[0] == "WORK")
			{
				array_push($workphone, explode("|",implode("|",$contact['TEL'][$i]))[1]);
			}
			elseif (explode(",",implode(",",$contact['TEL'][$i]))[0] == "HOME") {
				array_push($homephone, explode("|",implode("|",$contact['TEL'][$i]))[1]);
			}
		}

		$workadr = array();

		for($i = 0; $i < sizeof($contact['ADR']); $i++)
		{
			if(explode("|",implode("|",$contact['ADR'][$i]))[0] == "WORK")
			{
				array_push($workadr, explode("|",implode("|",$contact['ADR'][$i]))[1]);
			}
		}

		$arrWA = array();
		empty($workadr[0]) ?: $arrWA = explode(";", $workadr[0]); 

		$a = $this->config->getAppValue($this->appName, "spectacle");
		$spe = array();
		if(isset($a))
		{
			$spe = explode(";", $a);
		}

		$header  = array(
			"prenom",
			"nom",
			"societe",
			"titre",
			"Telephone bureau",
			"Telephone portable",
			"Adresse",
			"Code postal",
			"Ville",
			"Département",
			"Pays",
			"Contexte de représentation",
			"Contact prévu",
			"Note",
			"Suivi de prestations",
			"Historique"
		);
		if(isset($spe)) foreach($spe as $s) array_push($header, $s);

		$value = array(
			empty($contact['FN']) ? "" : explode(" ", $contact['FN'])[0],
			empty($contact['FN']) ? "" : explode(" ", $contact['FN'])[1],
			empty($contact['ORG']) ? "" : $contact['ORG'],
			empty($contact['TITLE']) ? "" :$contact['TITLE'],
			empty($workphone[0]) ? "" : $workphone[0],
			empty($homephone[0]) ? "" : $homephone[0],
			empty($arrWA[2]) ? "" : str_replace("\\", "", $arrWA[2]),
			empty($arrWA[5]) ? "" : str_replace("\\", "", $arrWA[5]),
			empty($arrWA[3]) ? "" : str_replace("\\", "", $arrWA[3]),
			empty($arrWA[4]) ? "" : str_replace("\\", "", $arrWA[4]),
			empty($arrWA[6]) ? "" : str_replace("\\", "", $arrWA[6]),
			empty($contact['CR']) ? "" : implode(" / ",explode(";", $contact['CR'])),
			empty($contact["CP"]) ? "" : date("d/m/Y", strtotime($contact["CP"])),
			empty($contact['NOTE']) ? "" : $contact['NOTE'],
			empty($contact['SP']) ? "" : $contact['SP'],
			empty($contact['HIST']) ? "" : $contact['HIST']
		);

		if(isset($spe)) foreach($spe as $s) array_push($value, $this->getEtatSpectacle($contact, $s));

		$filename = "contact_export_" . date("Y-m-d") . ".csv";

		$csv = array(
			$header,
			$value
		);

		$res = "";
        
        foreach($csv as $row)
        {

			$res = $res . implode(",", $row) . "\n";
			
		}
		

		return new CSVDownloadResponse($res, $filename, 'application/octet-stream');
	}

	/**
	 * Fonction permettant de récupérer la catégorie d'un spectacle dans un contact
	 */
	private function getEtatSpectacle($contact, $spectacle) : string
	{
		foreach(CONTACT::SPECTACLE_CAT as $specat)
		{
			if(in_array($spectacle, explode(";",  $contact[$specat[0]]))) return $specat[1];
		}
		return "pas d'avis";
	}
}