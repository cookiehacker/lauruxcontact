<?php

/**
 * Classe permettant d'avoir accés à des constantes liées aux contacts
 */

namespace OCA\LauruxContact\Constant;

final class Contact
{
    /**
     * Catégories des spectacles 
     * 
     * Clés : la clé de la catégorie du spectacle, de votre choix
     * Valeur : le nom de la catégorie
     */
    const SPECTACLE_CAT = [
        ["SPEI", "Intéressé"],
        ["SPEDP", "Déjà programmé"],
        ["SPEAD", "A déjà participé"],
        ["SPEDV", "Est déjà venu"],
        ["SPEED", "A envoyé une demande"],
        ["SPEPI", "Pas intéressé"]
    ];

    const CONTEXTESREPR = [
        "Salle",
        "Extérieur",
        "Rue",
        "Jeune publique",
        "Tout publique"
    ];
    
    const SEARCHPROPERTIES = [
        'UID',
        'PHOTO',
        'FN',
        'N',
        'BDAY',
        'ADR',
        'EMAIL',
        'TEL',
        'GENDER',
        'NOTE',
        'GEO',
        'RELATIONSHIP',
        'X-SOCIALPROFILE',
        'CATEGORIES',
        'ORG',
        'TITLE',
        'LANG',
        'NICKNAME',
        'TZ',
        'URL',
        'IMPP',
        'LABEL',
        'MAILER',
        'ROLE',
        'LOGO',
        'AGENT',
        'PRODID',
        'SORT-STRING',
        'SOUND',
        'URL',
        'VERSION',
        'CLASS',
        'KEY',
        'SPE'
    ];
}