<?php

/**
 * Classe permettant de creer une réponse HTTP conçu pour le téléchargement automatique d'un fichier CSV
 */

namespace OCA\LauruxContact\Http;

use OCP\AppFramework\Http\DownloadResponse;

class CSVDownloadResponse extends DownloadResponse 
{
  private $content;

  public function __construct(string $content, string $filename, string $contentType) {
    parent::__construct($filename, $contentType);

    $this->content = $content;

    $expires = new \DateTime('now + 11 months');
    $this->addHeader('Expires', $expires->format(\DateTime::RFC1123));
    $this->addHeader('Cache-Control', 'private');
    $this->addHeader('Pragma', 'cache');
  }
    
  public function render(): string {
		return $this->content;
	}
}