<?php

namespace OCA\LauruxContact\AppInfo;


use OCP\AppFramework\App;

class Application extends App {
	public function __construct (array $urlParams = array()) {
		parent::__construct('lauruxcontact', $urlParams);

		$container = $this->getContainer();
		$server = $container->getServer();
	}
}
