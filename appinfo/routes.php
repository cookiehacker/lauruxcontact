<?php
/**
 * Create your routes in here. The name is the lowercase name of the controller
 * without the controller part, the stuff after the hash is the method.
 * e.g. page#index -> OCA\LauruxContact\Controller\PageController->index()
 *
 * The controller class has to be registered in the application.php file since
 * it's instantiated in there
 */
return [
    'routes' => [
        ['name' => 'settings#configurationset',	        'url' => '/settings/configuration',	            'verb' => 'POST'],

	    ['name' => 'contact#index',                     'url' => '/',                                   'verb' => 'GET'],
        ['name' => 'contact#do_echo',                   'url' => '/echo',                               'verb' => 'POST'],
        ['name' => 'contact#syncContacts',              'url' => '/contact/sync',                       'verb' => 'POST'],
        ['name' => 'contact#syncOnlyContact',           'url' => '/contact/syncOnlyContact/{id}',       'verb' => 'POST'],
        ['name' => 'contact#getOnlyContact',            'url' => '/contact/getOnlyContact/{id}',        'verb' => 'POST'],
        ['name' => 'contact#ajoutSpectacle',            'url' => '/contact/ajoutSpectacle',             'verb' => 'POST'],
        ['name' => 'contact#filtrerContact',            'url' => '/contact/filtrerContact',             'verb' => 'POST'],
        ['name' => 'contact#filtrerContactSpe',         'url' => '/contact/filtrerContactSpe',          'verb' => 'POST'],
        ['name' => 'contact#annulerFiltre',             'url' => '/contact/annulerFiltre',              'verb' => 'POST'],
        ['name' => 'contact#suppSpectacle',             'url' => '/contact/suppSpectacle',              'verb' => 'DELETE'],
        ['name' => 'contact#suppContact',               'url' => '/contact/suppContact',                'verb' => 'DELETE'],
        ['name' => 'contact#saveInCSV',                 'url' => '/contact/saveInCSV',                  'verb' => 'GET'],
        ['name' => 'contact#importCSV',                 'url' => '/contact/importCSV',                  'verb' => 'POST'],
        ['name' => 'contact#filtrerContactFCR',         'url' => '/contact/filtrerContactFCR',          'verb' => 'POST'],
        ['name' => 'contact#filtrerContactSP',          'url' => '/contact/filtrerContactSP',           'verb' => 'POST'],

        ['name' => 'editcontact#index',                 'url' => '/editcontact/modifierContact/{id}',   'verb' => 'GET'],
        ['name' => 'editcontact#modifier',              'url' => '/editcontact/modifier',               'verb' => 'POST'],
        ['name' => 'editcontact#saveInCSV',             'url' => '/editcontact/saveInCSV/{id}',         'verb' => 'GET'],
        ['name' => 'editcontact#ajoutContactDansSpe',   'url' => '/editcontact/ajoutContactDansSpe',    'verb' => 'POST'],
        ['name' => 'editcontact#suppSpeDansContact',    'url' => '/editcontact/suppSpeDansContact',     'verb' => 'POST'],
                
        ['name' => 'addcontact#index',                  'url' => '/addcontact',                         'verb' => 'GET'],
        ['name' => 'addcontact#ajouterContact',         'url' => '/addcontact/ajouterContact',          'verb' => 'POST']
    ],
];
