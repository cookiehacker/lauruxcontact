<?php

$l = \OC::$server->getL10N('lauruxcontact');

\OC::$server->getNavigationManager()->add([
	'id' => 'lauruxcontact',
	'order' => 10,
	'href' => \OC::$server->getURLGenerator()->linkToRoute('lauruxcontact.contact.index'),
	'icon' => \OC::$server->getURLGenerator()->imagePath('lauruxcontact', 'app-dark.svg'),
	'name' => \OCP\Util::getL10N('lauruxcontact')->t('LauruxContact')
]);
