<?php
script('lauruxcontact', 'jquery-confirm');
script('lauruxcontact', 'bootstrap');
script('lauruxcontact', 'index');

style('lauruxcontact', 'jquery-confirm');
style('lauruxcontact', 'style');
style('lauruxcontact', 'index');
?>

<div id="lauruxcontact" class="section">
	<div class="index">
		<div class="titre">Filtres</div>
			<div class="btnFiltre">
				<input type="reset" class="resetFiltreGlobal" value="annuler le filtre"/>
				<input type="submit" class="exporterCsv" value="exporter en csv"/>
			</div>
		<div class="filtre">
			<div class="sousTitre">Filtre global</div>
			<input id="filtreText" type="text" placeholder="nom, email, etc..." />
			<input type="submit" class="filtrerContacts" value="<?php p($l->t('Filtrer')); ?>"/>

			<div class="sousTitre">Filtre suivi de prestation</div>
			<label>En cours</label>
			<input type="submit" class="filtrerSP" value="<?php p($l->t('Filtrer')); ?>"/>

			<div class="sousTitre">Filtre activité</div>
			<input id="spectacle" type="text" placeholder="Nom de l'activité"/>
			<input type="submit" class="submitSpectacle" value="<?php p($l->t('Ajouter')); ?>"/><br />
			<label id="error_label_spe"><?php p($l->t('')) ?></label>
			<label id="addContact"><?php p($l->t('')) ?></label></br>
			
			<input type="submit" class="resetFiltre" value="<?php p($l->t('Annuler le filtre')); ?>"/>
		</div>
		<input type="submit" class="ajoutContact" value="<?php p($l->t('Ajouter un contact')); ?>"/>
		<input type="submit" class="saveInCSV" value="<?php p($l->t('Exporter en CSV les contacts')); ?>"/>
		<img id='loader' style='display: none;' src="<?php print_unescaped(image_path('lauruxcontact', 'rolling.svg')); ?>" width='32px' height='32px'>
		<br />
		<form class="upload_csv" method="post" enctype="multipart/form-data">
			<input type="file" name="csv_file" class="csv_file"/>
			<input type="submit" name="upload" class="upload" value="<?php p($l->t('Importer les contacts en CSV')); ?>"/>
		</form>

		<br/>
		<div class="crfiltre"></div>
		<div class="contactFiltre">
			<h2><?php p($l->t('Vos contacts')) ?></h2>
			<?php foreach($_['contacts'] as $c) { ?>
				<div class="card border-primary mb-3" style="max-width: 20rem;">
					<div class="card-header"><b><?php strtoupper(p($c['FN'][0])); ?></b></div>
					<div class="card-body">
						<h4 class="card-title"><?php p($c['FN']); ?></h4>
						<?php if(sizeof($c['EMAIL'])>0){?>
							<p class="card-text">
								<ul>
									<?php foreach($c['EMAIL'] as $e){?>
										<?php if(strcmp($e, " ") !== 0) {?>
											<li><?php p($e) ?></li>
										<?php } ?>
									<?php } ?>
								</ul>
							</p> 
						<?php }?>
					</div>
					<input type="submit" class="modifierContact" id="<?php p($c['UID']) ?>" value="<?php p($l->t('Afficher / modifier le contact')); ?>"/>
					<input type="submit" class="suppContactModal" id="<?php p($c['UID']) ?>" value="<?php p($l->t('Supprimer le contact')); ?>"/>
				</div>
			<?php } ?>
		</div>
		

		<label for="spectacle"><?php p($l->t('Spectacles')) ?></label>
		<br />
		<br />

		<div class="row">
			<div class="spectacles">
				<?php foreach($_['spectacles'] as $s) { ?>
					<div class="card text-white bg-primary mb-3" style="max-width: 20rem; margin: 5px">
						<div class="card-body">
							<h4 class="card-title"><?php p($s); ?></h4>
							<input type="submit" class="suppModal" id="<?php p($s) ?>" value="<?php p($l->t('Supprimer le spectacle')); ?>"/>
							<input type="submit" class="filtreContactSpe" id="<?php p($s) ?>" value="<?php p($l->t('Filtrer les contacts')); ?>"/>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>
			
		<br/>

		<label for="spectacle"><?php p($l->t('Filtre contexte de représentation')) ?></label>
		<br />
		<br />

		<div class="row">
			<div class="fcr">
				<?php foreach($_['fcr'] as $s) { ?>
					<div class="card text-white bg-primary mb-3" style="max-width: 20rem; margin: 5px">
						<div class="card-body">
							<h4 class="card-title"><?php p($s); ?></h4>
							<input type="submit" class="filtrecontactfcr" id="<?php p($s) ?>" value="<?php p($l->t('Filtrer')); ?>"/>
						</div>
					</div>
				<?php } ?>
			</div>
		</div>

		<br />
		
		<div class="infosBd">
			<h2><?php p($l->t('Votre serveur')) ?></h2>
			<ul>
				<li><?php p($l->t('Database : ')) ?><?php p($_['database']) ?></li>
				<li><?php p($l->t('IP : ')) ?><?php p($_['ip']) ?></l1>
				<li><?php p($l->t('Port : ')) ?><?php p($_['port']) ?></li>
				<li><?php p($l->t('Nom d\'utilisateur : ')) ?><?php p($_['username']) ?></li>
			</ul>
			<?php p($l->t('Etat de connexion : ')) ?><?php p($_['dbVerif']) ?>

			<br/>	
			<?php p($l->t('Nombre de client : '))?><?php p($_['result']) ?>	

			<br />
			<label><b><?php p($_['newcontacts']) ?></b></label>
		</div>
	</div>

	<div class="modSuppSpe">
			
	</div>

	<div class="modSuppContact">	
	
	</div>
</div>



