<?php

\OCP\Util::addScript('lauruxcontact', 'admin');

/** @var $l \OCP\IL10N */
/** @var $_ array */

?>

<div id="lauruxcontact" class="section">
	<h2><?php p($l->t('Lauruxcontact')) ?></h2>
	<p>
        <label for="database"><?php p($l->t('Nom de la base de données')) ?></label>
		<br />
		<input id="database" type="text" value="<?php p($_['database']) ?>" />

        <br />

        <label for="ip"><?php p($l->t('Adresse IP')) ?></label>
		<br />
		<input id="ip" type="text" value="<?php p($_['ip']) ?>" />

        <br />

        <label for="port"><?php p($l->t('Port')) ?></label>
		<br />
		<input id="port" type="text" value="<?php p($_['port']) ?>" />

        <br />

        <label for="username"><?php p($l->t('Nom d\'utilisateur')) ?></label>
		<br />
		<input id="username" type="text" value="<?php p($_['username']) ?>" />

        <br />

        <label for="password"><?php p($l->t('Mot de passe')) ?></label>
		<br />
		<input id="password" type="password" value="<?php p($_['password']) ?>" />

        <br />

		<input type="submit" id="submitConfiguration" value="<?php p($l->t('Sauvegarder')); ?>"/>

		<br />
		<label id="error_label"><?php p($l->t('')) ?></label>
	</p>

</div>