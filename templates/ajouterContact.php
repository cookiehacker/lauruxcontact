<?php
script('lauruxcontact', 'ajoutercontact');
style('lauruxcontact', 'style');
?>

<div id="lauruxcontact" class="section">
    <input type="submit" class="retour" value="Précédent"/>
    <input type="submit" class="ajoutContact" value="Ajouter le contact"/>
    <label id="error_label"><?php p($l->t('')) ?></label>

    <div class="formAjoutContact">
        <div class="partieGauche">
            <h2>Ajout d'un contact</h2>

            <div class="titreForm">Information de base</div>

            <div class="preNom">
                <table>
                    <tr>
                        <td><input type="text" class="prenom" placeholder="Prénom"/></td>
                        <td><input type="text" class="nom" placeholder="Nom"/></td>
                    </tr>
                </table>
            </div>

            <div class="societeTitre">
                <div class=titreForm>Structure</div>
                <table>
                    <tr>
                        <td><input type="text" class="societe" placeholder="Société"/></td>
                        <td><input type="text" class="titre" placeholder="Titre"/></td>
                    </tr>
                </table>    
            </div>

            <div class="tel">
                <div class=titreForm>Téléphone</div>
                <table>
                    <tr>
                        <td><input type="text" class="telbureau" placeholder="Tel pro"/></td>
                        <td><input type="text" class="telportable" placeholder="Tel perso"/></td>
                    </tr>
                </table>
            </div>

            <div class="email">
                <div class=titreForm>Email</div>
                <table>
                    <tr>    
                        <td><input type="text" class="emailBureau" placeholder="Email pro"/></td>
                        <td><input type="text" class="emailPerso" placeholder="Email perso"/></td>
                    </tr>
                </table>
            </div>

            <div class="adresse">
                <div class=titreForm>Adresse</div>
                <table>
                    <tr>
                        <td><input type="text" class="addr" placeholder="Adresse"/></td>
                        <td><input type="text" class="cp" placeholder="Code postal"/></td>
                        <td><input type="text" class="ville" placeholder="Ville"/></td>
                    </tr>
                    <tr>
                        <td><input type="text" class="dep" placeholder="Département"/></td>
                        <td><input type="text" class="region" placeholder="Région"/></td>
                        <td><input type="text" class="pays" placeholder="Pays"/></td>
                    </tr>
                </table>
            </div>
        </div>
        <div class="partieDroite">
            <div class="suivi">
                <div class=titreForm>Suivi de prestation</div>
                <textarea class="suiviPContact"></textarea></br>
                <input type="checkbox" class="btn" name="sp" id="sp" value="en cours">En cours</input><br/>
            </div>

            <div class="proContact">
                <div class=titreForm>Prochain contact prévu</div>
                <input type="date" class="prochaineDateDeContact"/>
            </div>
            
            <div class="comm">
                <div class=titreForm>Notes - Commentaires</div>
                <textarea class="noteContact"></textarea></br>
            </div>

            <div class="histo">
                <div class=titreForm>Historique</div>
                <textarea class="historiqueContact"></textarea></br>
            </div>
        </div>
    </div>
</div>