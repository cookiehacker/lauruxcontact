<?php
script('lauruxcontact', 'modifiercontact');
style('lauruxcontact', 'style');
?>

<div id="lauruxcontact" class="section">
<h2>Modifier la fiche</h2>
<div class="formEditContact">
    <div class="partieGauche">
        <input type="submit" class="retour" value="Retour à l'accueil"/>
        <input type="submit" class="enregistrerModif" id='<?php p($_['contacts']['UID']) ?>' value="Enregistrer les modifications"/>
        <label id="error_label_edit"><?php p($l->t('')) ?></label>
        <input type="submit" class="saveInCSV" id='<?php p($_['contacts']['UID']) ?>' value="Sauvegarder en CSV"/>
        <input type="submit" class="syncContact" id='<?php p($_['contacts']['UID']) ?>' value="Synchroniser vers Laurux"/>
        <label id="error_label_sync"><?php p($l->t('')) ?></label>
            <div class="preNom">
                <table>
                    <tr>
                        <td><input type="text" class="prenom" placeholder="Prénom" value="<?php p(explode(" ", $_['contacts']['FN'])[1]) ?>"/></td>
                        <td><input type="text" class="nom" placeholder="Nom" value="<?php p(explode(" ", $_['contacts']['FN'])[0]) ?>"/></td>
                    </tr>
                </table>
            </div>

            <div class="societeTitre">
                <div class=titreForm>Structure</div>
                <table>
                    <tr>
                        <td><input type="text" class="societe" placeholder="Société" value="<?php p($_['contacts']['ORG']) ?>"/></td>
                        <td><input type="text" class="titre" placeholder="Titre" value="<?php p($_['contacts']['TITLE']) ?>"/></td>
                    </tr>
                </table>
            </div>

            <div class="tel">
                <div class=titreForm>Téléphone</div>
                <table>
                    <tr>
                        <td><input type="text" class="telbureau" placeholder="Tel pro" value="<?php p($_['workphone'])?>"/></td>
                        <td><input type="text" class="telportable" placeholder="Tel perso" value="<?php p($_['homephone'])?>"/></td>
                    </tr>
                </table>
            </div>
            
            <div class="email">
                <div class=titreForm>Email</div>
                <table>
                    <tr>    
                        <td><input type="text" class="emailbureau" placeholder="Email pro" value="<?php p($_['workemail'])?>"/></td>
                        <td><input type="text" class="emailperso" placeholder="Email perso" value="<?php p($_['homeemail'])?>"/></td>
                    </tr>
                </table>
            </div>

            <div class="adresse">
                <div class=titreForm>Adresse</div>
                <?php $arr = explode(";", $_['workadr']) ?>
                <table>
                    <tr>
                        <td><input type="text" class="addr" placeholder="Adresse" value="<?php p(str_replace("\\", "", $arr[2])) ?>"/></td>
                        <td><input type="text" class="cp" placeholder="Code postal" value="<?php p(str_replace("\\", "", $arr[5])) ?>"/></td>
                        <td><input type="text" class="ville" placeholder="Ville" value="<?php p(str_replace("\\", "", $arr[3])) ?>"/></td>
                    </tr>
                    <tr>
                        <td><input type="text" class="dep" placeholder="Département" value="<?php p(str_replace("\\", "", $arr[4])) ?> "/></td>
                        <td><input type="text" class="region" placeholder="Région" value=""/></td>
                        <td><input type="text" class="pays" placeholder="Pays" value="<?php p(str_replace("\\", "", $arr[6])) ?>"/></td>
                    </tr>
                </table>
            </div>

            <div class="contexte">
                <div class=titreForm>Contexte de représentation</div>
                <div class="btnContexte">
                    
                    <?php foreach($_['contexte'] as $c) { 
                        if(in_array($c, explode(";",$_['contacts']["CR"])))
                        {
                    ?>
                        <div class="centrage">
                            <input type="checkbox" class="btn" name="repr[]" id="test" value="<?php p($c) ?>" checked><?php p($c) ?></input><br/>
                        </div>
                        <?php } else { ?>
                        <div class="centrage">
                            <input type="checkbox" class="btn" name="repr[]" id="test" value="<?php p($c) ?>"><?php p($c) ?></input><br/>
                        </div>
                        <?php } ?>
                    <?php } ?>
                </div>
            </div>

        </div>
        <div class="partieDroite">

            <div class="lienActi">
                <div class="titreForm">Lien avec nos activités</div>

                <div class="form-group">
                    <label for="selectSpe">Le spectacle</label>
                    <select class="form-control" id="selectSpe">
                        <?php foreach($_['spectacles'] as $s) { ?>
                            <option value="<?php p($s) ?>"><?php p($s) ?></option>
                        <?php } ?>
                    </select>

                    

                    <select class="form-control" id="selectCat">
                        <?php foreach($_['cat'] as $cat) { ?>
                            <option value=<?php p($cat[0]) ?>><?php p($cat[1]) ?></option>
                        <?php } ?>
                    </select>
                </div>
                <input type="submit" class="spectacleContact" id="<?php p($_['contacts']['UID']) ?>" value="<?php p($l->t('Ajouter à un spectacle')); ?>"/>

                <table class="tabSpec">
                    <?php foreach($_['cat'] as $elem) {
                            foreach(explode(';',$_['spe'][$elem[0]]) as $e){
                                if($e != '') {
                                    if($elem[1] == "Intéressé") { ?>
                                        <tr>
                                            <td class="gras"><?php p($e) ?></td>
                                            <td class="gras"><?php p($elem[1]) ?></td>
                                            <td><input type="submit" class="suppSpe" id='<?php p($_['contacts']['UID'])?>|<?php p($e) ?>' value="Supprimer"/></td>
                                        </tr>
                                    <?php }
                                    else { ?>
                                        <tr>
                                            <td><?php p($e) ?></td>
                                            <td><?php p($elem[1]) ?></td>
                                            <td><input type="submit" class="suppSpe" id='<?php p($_['contacts']['UID'])?>|<?php p($e) ?>' value="Supprimer"/></td>
                                        </tr>
                                <?php } ?>
                            <?php } ?>
                        <?php } ?>
                    <?php } ?>
                </table>
            </div>
            
            <div class="suivi">
                <div class=titreForm>Suivi de prestation</div>
                <textarea class="suiviPContact"><?php p($_['contacts']['SP'])?></textarea></br>
                <?php if (intval($_['contacts']['ETSP']) == 1) { ?>
                    <input type="checkbox" class="btn" name="sp" id="sp" value="en cours" checked>En cours</input><br/>
                <?php } else { ?>
                    <input type="checkbox" class="btn" name="sp" id="sp" value="en cours">En cours</input><br/>
                <?php } ?>
            </div>

            <div class="proContact">
                <div class=titreForm>Prochain contact prévu</div>
                <input type="date" class="prochaineDateDeContact" id='<?php p($_['contacts']['UID'])?>' value='<?php p($_['date'])?>'/>
            </div>
            
            <div class="comm">
                <div class=titreForm>Notes - Commentaires</div>
                <textarea class="noteContact"><?php p($_['contacts']['NOTE'])?></textarea></br>
            </div>

            <div class="histo">
                <div class=titreForm>Historique</div>
                <textarea class="historiqueContact"><?php p($_['contacts']['HIST'])?></textarea></br>
            </div>
        </div>
    </div>
</div>