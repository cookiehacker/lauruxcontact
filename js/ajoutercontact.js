(function(){
    if(!OCA.LauruxContact) {
        /**
		 * Namespace for the files app
		 * @namespace OCA.LauruxContact
		 */
        OCA.LauruxContact = {};
    }

    /**
	 * @namespace OCA.LauruxContact.Ajoutercontact
	 */
    OCA.LauruxContact.Ajoutercontact = {
        initialize: function() {
            $('.retour').on('click', _.bind(this._clickRetour, this));
            $('.ajoutContact').on('click', _.bind(this._clickAjoutContact, this));
        },

        _clickAjoutContact : function()
        {
            OC.msg.startSaving('#error_label');
            
            if($(".nom").val() !== "" || $(".prenom").val() !== "")
            {
                $arrRepr = [];    
                $('input[name="repr[]"]:checked').each(function(){$arrRepr.push($(this).val())});
                
                $contact = new Map();
                $contact.set('FN', $(".nom").val() + " " + $(".prenom").val());
                $contact.set('ORG', $(".societe").val());
                $contact.set('TITLE', $(".titre").val());
                $contact.set('TEL;TYPE="WORK,VOICE"', $(".telbureau").val());
                $contact.set('TEL;TYPE="HOME,VOICE"', $(".telportable").val());
                $contact.set('EMAIL;TYPE=WORK', $(".emailbureau").val());
                $contact.set('EMAIL;TYPE=HOME', $(".emailperso").val());
                $contact.set('ADR;TYPE=WORK', ";;" + 
                                                $(".addr").val() + 
                                                ";" +
                                                $(".ville").val() + 
                                                ";" + 
                                                $(".dep").val() + 
                                                ";" + 
                                                $(".cp").val() + 
                                                ";" + 
                                                $(".pays").val());
                $contact.set('CR', $arrRepr.join(";"));
                $contact.set('CP', $(".date").val());
                $contact.set('NOTE', $(".noteContact").val());
                $contact.set('SP', $(".suiviPContact").val());
                $contact.set('ETSP', $('#sp').is(':checked') ? "1" : "0");
                $contact.set('HIST', $(".historiqueContact").val());

                $obj = Object.fromEntries($contact);

                console.log($obj);

                var request = $.ajax({
                    url: OC.generateUrl('/apps/lauruxcontact/addcontact/ajouterContact'),
                    type: 'POST',
                    data: {
                        contact : JSON.stringify($obj)
                    }
                });
                
                request.done(function (data) 
                {
                    OC.msg.finishedSuccess('#error_label', 'Sauvegardé !');
                    const obj = JSON.parse(data);
                    document.location.href=OC.generateUrl('/apps/lauruxcontact/editcontact/modifierContact/' + obj.contactID);
                });

                request.fail(function () {
                    OC.msg.finishedError('#error_label', 'Erreur !');
                });
            }
            else
            {
                OC.msg.finishedError('#error_label', 'Nom ou prénom indispensable !');
            }
        },

        _clickRetour : function()
        {
            document.location.href=OC.generateUrl('/apps/lauruxcontact/');
        },
        
    }
})();

$(document).ready(function(){
    OCA.LauruxContact.Ajoutercontact.initialize();
});
