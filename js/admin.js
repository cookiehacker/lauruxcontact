(function() {
	if (!OCA.LauruxContact) {
		/**
		 * Namespace for the files app
		 * @namespace OCA.LauruxContact
		 */
		OCA.LauruxContact = {};
	}

	/**
	 * @namespace OCA.LauruxContact.Admin
	 */
	OCA.LauruxContact.Admin = {
		initialize: function() {
			$('#submitConfiguration').on('click', _.bind(this._onClickSubmitConfiguration, this));
		},

		_onClickSubmitConfiguration: function () {
			OC.msg.startSaving('#error_label');

			var request = $.ajax({
				url: OC.generateUrl('/apps/lauruxcontact/settings/configuration'),
				type: 'POST',
				data: {
                    database: $('#database').val(),
                    ip: $('#ip').val(),
                    username: $('#username').val(),
					password: $('#password').val(),
					port: $('#port').val()
				}
			});

			request.done(function (data) {
				$('#database').val(data.database);
				$('#ip').val(data.ip);
				$('#username').val(data.username);
				$('#password').val(data.password);
				$('#port').val(data.port);
				OC.msg.finishedSuccess('#error_label', 'Sauvegardé !');
			});

			request.fail(function () {
				OC.msg.finishedError('#error_label', 'Erreur !');
			});
		}
	}
})();

$(document).ready(function() {
	OCA.LauruxContact.Admin.initialize();
});
