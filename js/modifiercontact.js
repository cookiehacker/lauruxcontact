(function(){
    if(!OCA.LauruxContact) {
        /**
		 * Namespace for the files app
		 * @namespace OCA.LauruxContact
		 */
        OCA.LauruxContact = {};
    }

    /**
	 * @namespace OCA.LauruxContact.Modifiercontact
	 */
    OCA.LauruxContact.Modifiercontact = {
        initialize: function() {
            $('.retour').on('click', _.bind(this._clickRetour, this));
            $('.afficherContact').on('click', _.bind(this._clickAfficherContact, this));
            
            $('.suppSpe').on('click', _.bind(this._clickSuppSpe, this));

            $('.enregistrerModif').on('click', _.bind(this._clickEnregistrerModif, this));
            
            $('.spectacleContact').on('click', _.bind(this._clickContactDansSpe, this));
            
            $('.syncContact').on('click', _.bind(this._clickSyncContact, this));
            $('.saveInCSV').on('click', _.bind(this._clickSaveInCSV, this));
        },

        _clickEnregistrerModif : function(e)
        {
            OC.msg.startSaving('#error_label_edit');
            
            if($(".nom").val() !== "" || $(".prenom").val() !== "")
            {
                $arrRepr = [];    
                $('input[name="repr[]"]:checked').each(function(){$arrRepr.push($(this).val())});
                
                $contact = new Map();
                $contact.set('FN', $(".nom").val() + " " + $(".prenom").val());
                $contact.set('ORG', $(".societe").val());
                $contact.set('TITLE', $(".titre").val());
                $contact.set('TEL;TYPE="WORK,VOICE"', $(".telbureau").val());
                $contact.set('TEL;TYPE="HOME,VOICE"', $(".telportable").val());
                $contact.set('EMAIL;TYPE=WORK', $(".emailbureau").val());
                $contact.set('EMAIL;TYPE=HOME', $(".emailperso").val());
                $contact.set('ADR;TYPE=WORK', ";;" + 
                                                $(".addr").val() + 
                                                ";" +
                                                $(".ville").val() + 
                                                ";" + 
                                                $(".dep").val() + 
                                                ";" + 
                                                $(".cp").val() + 
                                                ";" + 
                                                $(".pays").val());
                $contact.set('CR', $arrRepr.join(";"));
                $contact.set('CP', $(".prochaineDateDeContact").val());
                $contact.set('NOTE', $(".noteContact").val());
                $contact.set('SP', $(".suiviPContact").val());
                $contact.set('ETSP', $('#sp').is(':checked') ? "1" : "0");
                $contact.set('HIST', $(".historiqueContact").val());

                $obj = Object.fromEntries($contact);

                console.log($obj);

                var request = $.ajax({
                    url: OC.generateUrl('/apps/lauruxcontact/editcontact/modifier'),
                    type: 'POST',
                    data: {
                        contactedited : JSON.stringify($obj),
                        utilisateur : e.target.id
                    }
                });
                
                request.done(function (data) 
                {
                    OC.msg.finishedSuccess('#error_label_edit', 'Sauvegardé !');
                });

                request.fail(function () {
                    OC.msg.finishedError('#error_label_edit', 'Erreur !');
                });
            }
            else
            {
                OC.msg.finishedError('#error_label_edit', 'Nom ou prénom indispensable !');
            }
        },

        _clickSaveInCSV : function(e)
        {
            document.location.href=OC.generateUrl('/apps/lauruxcontact/editcontact/saveInCSV/'+e.target.id);        
        },

        _clickSyncContact: function(e) {
            OC.msg.startSaving('#error_label_sync');

            var request = $.ajax({
                url: OC.generateUrl('/apps/lauruxcontact/contact/syncOnlyContact/' + e.target.id),
                type: 'POST'
            });

            request.done(function(){
                OC.msg.finishedSuccess('#error_label_sync', 'Contact synchronisé !');
            });

            request.fail(function() {
                OC.msg.finishedError('#error_label_sync', 'Erreur !');
            });
        },

        _clickSuppSpe : function(e)
        {
            $list = e.target.id.split("|");
            var request = $.ajax({
				url: OC.generateUrl('/apps/lauruxcontact/editcontact/suppSpeDansContact'),
				type: 'POST',
				data: {
                    utilisateur: $list[0],
                    spectacle: $list[1],
				}
			});

			request.done(function (data) {
                const obj = JSON.parse(data);
                if(obj.status == 'ok')
                {
                    $('.tabSpec').html('');

                    obj.cat.forEach(element => {
                        if(obj[element[0]] !== undefined)
                        {
                            $list = obj[element[0]].split(";");
                            refreshSpectacle('.tabSpec', element[1], $list, obj.id)
                        } 
                        
                    });
                    

                    OCA.LauruxContact.Modifiercontact.initialize();
                }
			});
        },

        _clickAfficherContact : function(e)
        {
            document.location.href=OC.generateUrl('/apps/lauruxcontact/showcontact/afficherContact/' + e.target.id);
        },

        _clickRetour : function()
        {
            document.location.href=OC.generateUrl('/apps/lauruxcontact/');
        },

        _clickContactDansSpe: function(e)
        {
            OC.msg.startSaving('#addContact');

            console.log($("#selectSpe").find(":selected").val());
            console.log($("#selectCat option:selected").val());
            console.log($("#selectCat option:selected").text());

            var request = $.ajax({
				url: OC.generateUrl('/apps/lauruxcontact/editcontact/ajoutContactDansSpe'),
				type: 'POST',
				data: {
                    utilisateur: e.target.id,
                    spectacle: $( "#selectSpe option:selected" ).val(),
                    cat: $("#selectCat option:selected").val()
				}
			});

			request.done(function (data) {
                OC.msg.finishedSuccess('#addContact', 'Sauvegardé !');
                const obj = JSON.parse(data);
                if(obj.status == 'ok')
                {
                    console.log("ajouté : " + obj.id);

                    $('.tabSpec').html('');

                    obj.cat.forEach(element => {
                        if(obj[element[0]] !== undefined)
                        {
                            $list = obj[element[0]].split(";");
                            refreshSpectacle('.tabSpec', element[1], $list, obj.id)
                        } 
                        
                    });

                    OCA.LauruxContact.Modifiercontact.initialize();
                }
            });
            
            request.fail(function () {
				OC.msg.finishedError('#error_label', 'Erreur !');
			});
        },

    }
})();

function refreshSpectacle(divname, texte, list, id)
{
    if(texte === "Intéressé")
    {
        list.forEach(elem => $(divname).append(elem != '' ? `
        <tr>
            <td class="gras">`+elem+`</td>
            <td class="gras">`+texte+`</td>
            <td><input type="submit" class="suppSpe" id="`+id+`|`+elem+`" value="Supprimer"/></td>
        </tr>
        `: ''))
    }
    else
    {
        list.forEach(elem => $(divname).append(elem != '' ? `
        <tr>
            <td>`+elem+`</td>
            <td>`+texte+`</td>
            <td><input type="submit" class="suppSpe" id="`+id+`|`+elem+`" value="Supprimer"/></td>
        </tr>
        `: ''))
    }
    
}

$(document).ready(function(){
    OCA.LauruxContact.Modifiercontact.initialize();
});

//bouton fixé
var btn = $('#button');

$(window).scroll(function() {
  if ($(window).scrollTop() > 300) {
    btn.addClass('show');
  } else {
    btn.removeClass('show');
  }
});

btn.on('click', function(e) {
  e.preventDefault();
  $('html, body').animate({scrollTop:0}, '300');
});
