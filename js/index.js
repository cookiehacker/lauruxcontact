(function(){
    if(!OCA.LauruxContact) {
        /**
		 * Namespace for the files app
		 * @namespace OCA.LauruxContact
		 */
        OCA.LauruxContact = {};
    }

    /**
	 * @namespace OCA.LauruxContact.Index
	 */
    OCA.LauruxContact.Index = {
        initialize: function() {
            $('.ajoutContact').on('click', _.bind(this._clickAjoutContact, this));
            $('.submitSpectacle').on('click', _.bind(this._clickSubmitSpectacle, this));
            $('.filtrerContacts').on('click', _.bind(this._clickFiltrerContacts, this));
            $('.filtreContactSpe').on('click', _.bind(this._clickFiltrerContactSpe, this));
            $('.resetFiltre').on('click', _.bind(this._clickesetFiltre, this));
            $('.afficherContact').on('click', _.bind(this._clickAfficherContact, this));
            $('.modifierContact').on('click', _.bind(this._clickModifierContact, this));
            $('.suppModal').on('click', _.bind(this._clickSuppModal, this));
            $('.suppContactModal').on('click', _.bind(this._clickSuppContactModal, this));
            $('.saveInCSV').on('click', _.bind(this._clickSaveInCSV, this));
            $('.upload_csv').on('submit', _.bind(this._clickImportCSV, this));
            $('.filtrecontactfcr').on('click', _.bind(this._clickfiltrecontactfcr, this));
            $('.filtrerSP').on('click', _.bind(this._clickfiltreSP, this))
        },

        _clickfiltreSP : function()
        {
            var request = $.ajax({
				url: OC.generateUrl('/apps/lauruxcontact/contact/filtrerContactSP'),
				type: 'POST'
            });
            
            request.done(function (data) {
                const obj = JSON.parse(data);
                
                if(obj.status == 'ok') 
                {
                    if(obj.contact)
                    {
                        $('.crfiltre').html('<label>Suivi de prestation : en cours</label>');
                        refreshContact(obj.contact);
                    }
                }
                    
                OCA.LauruxContact.Index.initialize();
            });
        },

        _clickfiltrecontactfcr : function(e)
        {
            var request = $.ajax({
				url: OC.generateUrl('/apps/lauruxcontact/contact/filtrerContactFCR'),
				type: 'POST',
				data: {
                    rep: e.target.id
				}
            });
            
            request.done(function (data) {
                const obj = JSON.parse(data);
                
                if(obj.status == 'ok') 
                {
                    if(obj.contact)
                    {
                        $('.crfiltre').html('<label>Contexte de représentation : '+obj.rep+'</label>');
                        refreshContact(obj.contact);
                    }
                }
                    
                OCA.LauruxContact.Index.initialize();
            });
        },

        _clickImportCSV : function(e)
        {
            e.preventDefault();
            var formData = new FormData();
            console.log($('.csv_file').files)
            formData.append("csv", $('.csv_file').files[0])
            console.log(formData)
            var request = $.ajax({
				url: OC.generateUrl('/apps/lauruxcontact/contact/importCSV'),
				type: 'POST',
                data: { csv : formData },
                dataType:'json',
                contentType:false,
                cache:false,
                processData:false
			});

			request.done(function (data) {
                OC.msg.finishedSuccess('#error_label_spe', 'Sauvegardé !');
			});

			request.fail(function () {
				OC.msg.finishedError('#error_label_spe', 'Erreur !');
			});
        },

        _clickSaveInCSV : function()
        {
            //document.location.href=OC.generateUrl('/apps/lauruxcontact/contact/saveInCSV'); 
            $.ajax({
                url: OC.generateUrl('/apps/lauruxcontact/contact/saveInCSV'),
                type: 'GET',
                beforeSend: function(){
                 $("#loader").show();
                },
                success : function(){
                    document.location.href=OC.generateUrl('/apps/lauruxcontact/contact/saveInCSV'); 
                },
                complete:function(){
                 $("#loader").hide();
                }
            });       
        },

        _clickSuppContactModal : function(e)
        {
            $jc = $.confirm({
                title: 'Supprimer un contact',
                content: 'Êtes-vous sûr de vouloir supprimer le contact ?',
                buttons: {
                    confirmer: function () {
                        var request = $.ajax({
                            url: OC.generateUrl('/apps/lauruxcontact/contact/suppContact'),
                            type: 'DELETE',
                            data: {
                                utilisateur: e.target.id,
                            }
                        });
            
                        request.done(function (data) {
                            const obj = JSON.parse(data);
                            if(obj.status == 'ok') refreshContact(obj.contact);
                        });
                    },
                    annuler: function () {
                        
                    },
                }
            });

            $jc.setTheme("Supervan")
        },

        _clickAjoutContact : function()
        {
            document.location.href=OC.generateUrl('/apps/lauruxcontact/addcontact');
        },

        _clickSuppModal : async function(e)
        {
            $jc = $.confirm({
                title: 'Supprimer un spectacle',
                content: 'Êtes-vous sûr de vouloir supprimer le spectacle <b>'+e.target.id+'</b> ?',
                buttons: {
                    confirmer: function () {
                        OC.msg.startSaving('#error_label_spe');

                        $('.modSuppSpe').html("");

                        var request = $.ajax({
                            url: OC.generateUrl('/apps/lauruxcontact/contact/suppSpectacle'),
                            type: 'DELETE',
                            data: {
                                spectacle: e.target.id,
                            }
                        });

                        request.done(function (data) {
                            const obj = JSON.parse(data);
                            if(obj.status == 'ok')
                            {
                                $('.spectacles').html('');
                                $list = obj.spectacle.split(";");

                                $list.forEach(elem => $('.spectacles').append(elem != '' ? `
                                    <div class="card text-white bg-primary mb-3" style="max-width: 20rem; margin: 5px">
                                        <div class="card-body">
                                            <h4 class="card-title">`+elem+`</h4>
                                            <input type="submit" class="suppModal" id="`+elem+`" value="Supprimer le spectacle"/>
                                            <input type="submit" class="filtreContactSpe" id="`+elem+`" value="Filtrer les contacts"/>
                                        </div>
                                    </div>
                                `: ''))

                                refreshContact(obj.contact);

                                OC.msg.finishedSuccess('#error_label_spe', 'Supprimer !');

                                $('#suppMod').modal('hide');

                            }
                        });
                        
                        request.fail(function () {
                            OC.msg.finishedError('#error_label_spe', 'Erreur !');
                        });
                    },
                    annuler: function () {
                        
                    },
                }
            });

            $jc.setTheme("Supervan")
        },

        _clickModifierContact : function(e)
        {
            document.location.href=OC.generateUrl('/apps/lauruxcontact/editcontact/modifierContact/' + e.target.id);
        },

        _clickAfficherContact : function(e)
        {
            document.location.href=OC.generateUrl('/apps/lauruxcontact/showcontact/afficherContact/' + e.target.id);
        },

        _clickesetFiltre: async function(e)
        {
            var request = $.ajax({
				url: OC.generateUrl('/apps/lauruxcontact/contact/annulerFiltre'),
				type: 'POST',
            });

            request.done(function (data) {
                const obj = JSON.parse(data);
                if(obj.status == 'ok') 
                {
                    refreshContact(obj.contact);
                    $('.crfiltre').html('');
                }
                
            });
        },

        _clickFiltrerContacts: async function(e)
        {
            var request = $.ajax({
				url: OC.generateUrl('/apps/lauruxcontact/contact/filtrerContact'),
				type: 'POST',
				data: {
                    filtre: $('#filtreText').val()
				}
            });
            
            request.done(function (data) {
                const obj = JSON.parse(data);
                if(obj.status == 'ok') 
                {
                    refreshContact(obj.contact, obj.spectacles); 
                    $('.crfiltre').html('');
                }
                
            });
        },

        _clickFiltrerContactSpe: async function(e)
        {
            var request = $.ajax({
				url: OC.generateUrl('/apps/lauruxcontact/contact/filtrerContactSpe'),
				type: 'POST',
				data: {
                    spectacle: e.target.id
				}
            });
            
            request.done(function (data) {
                const obj = JSON.parse(data);
                
                if(obj.status == 'ok') 
                {
                    $('.contactFiltre').html('<h2>Vos contacts</h2>');

                    obj.cat.forEach(element => {
                        $('.contactFiltre').append('<div class="filtre'+element[0]+'"></div>')

                        $contact = obj['contact'][element[0]];

                        $('.filtre'+element[0]).append(`<h3>`+element[1]+`</h3>`);
                        filtre($contact, '.filtre'+element[0]);

                        $('.crfiltre').html('');
                    });
                }
                    
                OCA.LauruxContact.Index.initialize();
    
                
                
            });
        },

        _clickSubmitSpectacle: function()
        {
            OC.msg.startSaving('#error_label_spe');

			var request = $.ajax({
				url: OC.generateUrl('/apps/lauruxcontact/contact/ajoutSpectacle'),
				type: 'POST',
				data: {
                    spectacle: $('#spectacle').val(),
				}
			});

			request.done(function (data) {
                const obj = JSON.parse(data);
                if(obj.status == 'ok')
                {
                    $('.spectacles').html('');
                    $list = obj.spectacle.split(";");

                    $list.forEach(elem => $('.spectacles').append(elem != '' ? `
                        <div class="card text-white bg-primary mb-3" style="max-width: 20rem; margin: 5px">
                            <div class="card-body">
                                <h4 class="card-title">`+elem+`</h4>
                                <input type="submit" class="suppModal" id="`+elem+`" value="Supprimer le spectacle"/>
                                <input type="submit" class="filtreContactSpe" id="`+elem+`" value="Filtrer les contacts"/>
                            </div>
                        </div>
                    `: ''));


                    refreshContact(obj.contact);

                    OC.msg.finishedSuccess('#error_label_spe', 'Sauvegardé !');
                }

			});

			request.fail(function () {
				OC.msg.finishedError('#error_label_spe', 'Erreur !');
			});
        },

        
    }
})();

function filtre(contacti, idDiv)
{
    for(var i in contacti)
    {
                        
        $np = (contacti[i][0] !== null ? contacti[i][0] : "Aucun prenom et nom");
        $uid = (contacti[i][1] !== null ? contacti[i][1] : "Aucun UID");
                        
        if(contacti[i][2] !== null)
        {
                                    
            $mail = '';
            for(var j in contacti[i][2])
            {
                $mail = $mail + "<p>" + contacti[i][2][j]['value'] + "</p>";
            }
        }
        else
        {
            $mail = "Aucun email";
        }
                                                        
        $(idDiv).append(`
            <div class="card border-primary mb-3" style="max-width: 20rem;">
                <div class="card-header"><b>`+$np[0]+`</b></div>
                <div class="card-body">
                    <h4 class="card-title">`+$np+`</h4>
                    <p class="card-text">`+$mail+`</p>
                </div>
                <input type="submit" class="afficherContact" id="`+$uid+`" value="Afficher le contact"/>
                <input type="submit" class="modifierContact" id="`+$uid+`" value="Modifier le contact"/>
                <input type="submit" class="suppContactModal" id="`+$uid+`" value="Supprimer le contact"/>
            </div>
        `);        
    }
}

function refreshContact(contact)
{
    $('.contactFiltre').html('');
    $('.contactFiltre').append('<h2>Vos contacts</h2>');
    for(var i in contact)
    {
        $np = (contact[i][0] !== null ? contact[i][0] : "Aucun prenom et nom");

        $uid = (contact[i][1] !== null ? contact[i][1] : "Aucun UID");

        if(contact[i][2] !== null)
        {
            
            $mail = '';
            for(var j in contact[i][2])
            {
                $mail = $mail + "<p>" + contact[i][2][j]['value'] + "</p>";
            }
        }
        else
        {
            $mail = "Aucun email";
        }

        $('.contactFiltre').append(`
            <div class="card border-primary mb-3" style="max-width: 20rem;">
                <div class="card-header"><b>`+$np[0]+`</b></div>
                <div class="card-body">
                    <h4 class="card-title">`+$np+`</h4>
                    <p class="card-text">`+$mail+`</p>
                </div>
                <input type="submit" class="modifierContact" id="`+$uid+`" value="Afficher / modifier le contact"/>
                <input type="submit" class="suppContactModal" id="`+$uid+`" value="Supprimer le contact"/>
            </div>
            `);                       
    }
    OCA.LauruxContact.Index.initialize();
}

$(document).ready(function(){
    OCA.LauruxContact.Index.initialize();
});
